.global main

// Entrée: lit deux entiers positifs de 64 bits: a, b
// Sorties: Quantité de nombres premier dans [a,b] où 2 <= a <= b
//          Quantité de nombres premier palindromes dans [a,b]
// Usage des registres:
//   x19 -- a  ...
main:
        // Lire a                       //
        adr   x0, fmtEntree             //
        adr   x1, nombre                //
        bl    scanf                     // scanf(&fmtEntree, &nombre)
        ldr   x19, nombre               // a = nombre

        // Lire b
        adr   x0, fmtEntree             //
        adr   x1, nombre                //
        bl    scanf                     // scanf(»&fmtEntree, &nombre)
        ldr   x21, nombre               // b = nombre1

		mov   x26,2                     //constante 2
		mov   x27,10                    //constante 10
		mov   x25,0                     //cmpPremiers=0
		mov   x28,0                     //palindrome=0

		cmp   x19,x26                   //Si a==2
		b.ne  nbPair                    //Si a!=2 aller à boucle
		add   x25,x25,1                 //cmpPremiers++
		add   x28,x28,1                 //palindrome++
nbPair:
		tbnz  x19,0,boucle              //Si a pair
		add   x19,x19,1                 //a++
//while(a<b)
boucle:
		cmp   x19, x21                  //Si a < b
		b.hi  affichage                 //Sinon affichage
		mov   x22,3                     //i=3
		mov   x20,x22                   //j=i=3

//Calcul de la racine carré du nombre
racine:
		mul   x23,x20,x20               //x=racine^2
		cmp   x19,x23                   //Si a >= x (x=racine^2)
		b.lo  soustraction              //Si a < x (x=racine^2) aller soustraction
		add   x20,x20,1                 //racine++
		b     racine                    //Recommence racine
//Réduire la racine de 1
soustraction:
		sub   x20,x20,1                 //racine--

//for(i=3;i<=a/2;i+=2)
boucleFor:
		cmp   x20,x22                   //Si racine >= i
		b.lo  nbPremier                 //Si racine < i aller à nombre premier
		udiv  x23,x19,x22               //resDiv = a / i
		msub  x24,x22,x23,x19           //mod=a-(resDiv*i)
        cbz   x24,boucleSuite           //Si mod==0 sortir du for
		add   x22,x22,2                 //i+=2
		b     boucleFor

//En cas de nombre premier
nbPremier:
        add   x25,x25,1                 //cmpPremiers++
		mov   x24,0                     //Palin=0
		mov   x22,x19                   //c=a

//Verification palindrome
palindrome:
		udiv  x26,x22,x27               //resDiv = c / 10
		msub  x23,x26,x27,x22           //mod= c -(resDiv*10)
		madd  x24,x24,x27,x23           //palin= mod + (palin*10)
		mov   x22,x26                   //c=resDiv
		cbnz  x26,palindrome            //Si resDiv!=0 retour à palindrome

//Comparer le nombre à son inverse
comparaison:
		cmp   x24,x19                   //Si palin=a
		b.ne  boucleSuite               //Si palin!=a aller boucleSuite
		add   x28,x28,1                 //cmpPalindrome++

boucleSuite:
        add   x19,x19,2                 //a++
		b     boucle                    //Retour à boucle

affichage:
		adr   x0, sortie
		mov   x1,x25
		bl    printf

		adr   x0, sortie
		mov   x1,x28
		bl    printf

q:
        mov     x0, 0
        bl      exit

/*
    données ici au besoin
                           */
.section ".bss"
                .align  8
nombre:         .skip   8

.section ".rodata"
fmtEntree:    .asciz    "%lu"
sortie:       .asciz    "%lu\n"
